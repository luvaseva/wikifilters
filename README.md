# Wikifilters

This repository contains my master's thesis on EN Wikipedia's edit filter system.

## Structure

* `defense` contains my thesis's defense presentation
* `filter-lists` contains different filter list views meant to facilitate the inquiry
* `meeting-notes` contains notes from discussing the progress with my supervisor
* `memos` contains some short written artefacts on singular topics/ideas. The approach is borrowed from Grounded Theory research
* `opensym-paper` contains the sources for an article and the corresponding presentation thereof submitted to the [OpenSym2020](https://opensym.org/os2020/proceedings/)
* `quarries` contains a number of queries to [https://quarry.wmflabs.org/](https://quarry.wmflabs.org/), a web service which allows running SQL queries against Wikipedia and other Wikimedia projects
* `research-group-presi` contains two progress presentations in front of the research group who supervised my thesis
* `src` contains some random scripts and a jupyter notebook used for data exploration. This is the starting point for anyone who wishes to verify my analyses or build their owns reusing the results I've obtained
* `thesis` contains the actual thesis

## License

### Code

The code in this repository is licensed under the MIT license.
View [LICENSE](LICENSE.md) for details.

### Text

All text artefacts (including presentations, etc.) are licensed under the Creative Commons' [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
