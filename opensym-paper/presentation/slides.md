% You shall not publish: Edit filters on EN Wikipedia
% OpenSym 2020
% Lyudmila Vaseva and Claudia Müller-Birn

---

## Overview

* Motivation
* What is an edit filter
* Research questions
* Findings
* Directions for future studies

---

## Motivation

---

<img src="images/mechanisms-no-filters.png" alt="All quality control mechanisms ordered by mean response time" height="420" class="stretch"> 

---

## What is an edit filter

* MediaWiki extension
* regex similar pattern
* triggered upon edit, account creation, upload, etc.
* can trigger logging, tagging, warning or disallowing the action

---

## Research questions

* Q1: What is the role of edit filters among existing quality control mechanisms?
* Q2: How are edit filters governed?
* Q3: What do filters do? 
* Q4: How has their use changed over time?

---

## Q1: What is the role of edit filters among existing algorithmic quality-control mechanisms on Wikipedia?

---

<img src="images/mechanisms.png" alt="All quality control mechanisms ordered by mean response time" height="420" class="stretch">


---

## Q2: How are edit filters governed?

* EN Wikipedia: edit filter managers
* best practices

---

## Q3: Which type of tasks do filters take over?

---

<img src="images/public-filters-bar_and_hidden-filters-bar.png" alt="There are 954 edit filters on EN Wikipedia: roughly 21% of them
are active, 16% are disabled, and 63% are deleted" height="300" class="stretch">

---

<img src="images/active-filter-actions-horizontal-stacked-bar.png" alt="Filter actions for enabled filters" height="420" class="stretch">


---

<img src="images/manually-assigned-lables-bar.png" alt="Distribution of manually assigned labels" height="420" class="stretch">

---

## Q4: How has filters' use evolved over time?

---

<img src="images/manual-tags-histogram-new-axes-feb2019.png" alt="Number of filter hits per month, according to manuall assigned labels" height="400" class="stretch">

---

# So what

---

### Directions for future studies

* Comparison across different language versions: different types of governance? Different quality control ecosystems?
* Ethnographic analysis: Repercussions on (new) editors


---

# Thank you!

These slides are licensed under the [CC BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

![by](images/Cc-by_new_white.png)
![sa](images/Cc-sa_white.png)

The project is available under: [https://git.imp.fu-berlin.de/luvaseva/wikifilters](https://git.imp.fu-berlin.de/luvaseva/wikifilters)

---

# Questions? Comments? Thoughts?

---

<img src="images/number-edits-over-the-years.png" alt="Number of edits over the years">

---

<img src="images/reverts.png" alt="Number of reverts per month, Jul 2001-Jul 2017">
<small>Data source: R.S. Geiger and A. Halfaker. 2017. Code and Datasets for: Operationalizing Conflict and Cooperation Between Automated Software Agents in Wikipedia. Figshare (2017). https://doi.org/10.6084/m9.figshare.5362216</small>

---

<img src="images/filter-hits-editor-actions.png" alt="Number of filter hits per month, according to causing editor's action" height="250">

---

## What is an edit filter

<img src="images/Screenshot-warning-shouting-new.png" alt="Screenshot of a warning message caused by triggering edit filter #50 (Shouting)">
