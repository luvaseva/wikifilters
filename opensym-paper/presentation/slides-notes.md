% You shall not publish: Edit filters on EN Wikipedia
% OpenSym 2020
% Lusy and clmb

---

## Overview

* Motivation
* Research Questions
* What is an edit filter
* Findings
* Directions for future studies
* Contributions

---

## Motivation


Notes
The idea for the current project was born at the end of 2018: just couple of months before the EU parliament adopted its new copyright directive (TODO: Check name!)
A hotly debated part of this law were so called upload filters which internet plattforms which allowed the upload of user generated content were supposed to introduce in order to ensure quality of the content and prevent copyright infringements.


<img src="images/editors-rise-decline.png" height="500" alt="Rise and decline in numbers of editors on EN Wikipedia">
<small>Source: Halfaker et al. "The Rise and Decline of an Open Collaboration System: How Wikipedia’s reaction to popularity is causing its decline"</small>


* wikipedia is a complex socio technical system
* we have the luck it's "open", so we can study it and learn how things work and
apply the insights to less open systems
* "anyone can edit": increasing popularity in 2006; -> increasing need for
  quality control
* edit filters a one particular mechanism for quality control among several,
  and one previously unstudied
* seem relevant to understand how and what they do since they make it possible
  to disallow edits (and other actions, but above all edits) from the very
  beginning

---

## What is an edit filter

Notes:

* Screenshot of filter message
* have a look at the corresponding filter's detailed page

Sumup:
* MediaWiki extension
* regex based filtering of edits and other actions (e.g. account creation, page deletion or move, upload)
* triggers *before* an edit is published
* different actions can be defined: warn, tag, disallow most common

---

## Research questions

* What is the role of edit filters among existing quality control mechanisms?
* How are edit filters governed?
* What do filters do? 
* How has their use changed over time?

---

## Analysis Sources

<img src="images/icon-literature.png" class="stretch" height="500" alt="Literature">
<img src="images/icon-documentation.png" class="stretch" height="500" alt="Documentation">
<img src="images/icon-data.png" class="stretch" height="500" alt="Data">

Notes:
* Literature
* Documentation
* Data (Edit filter patterns, DB log table)
* Analysis sources
  * State of the literature/Literature: What does the scientific community know
  * Documentation: What is an edit filter and why was it introduced according to Wikipedia's/MediaWiki pages/Wikipedia's community?
  * Data Analysis: Edit filters on English Wikipedia

---

## Q1: What is the role of edit filters among existing algorithmic quality-control mechanisms on Wikipedia?


<img src="images/mechanisms.pdf" class="stretch" height="500" alt="All quality control mechanisms ordered by mean response time">

Notes:

Briefly introduce other mechanisms according to literature; -> filters are missing

Then, situate filters:
* 1st mechanism activated to control quality (at the beginning of the funnel)
* historically: faster, by being a direct part of the core software: disallow
  even before publishing
* can target malicious users directly without restricting everyone (<-> page
  protection)
* introduced to take care of obvious but cumbersome to remove vandalism
* people fed up with bot introduction and development processes (poor quality, no tests, no
  code available for revision in case of problems) (so came up with a new approach)
* disallow certain types of obvious pervasive (perhaps automated) vandalism directly
* takes more than a single click to revert
* human editors can use their time more productively elsewhere

---

* edit filters triggered *before* an edit is published
* disallow certain types of obvious, pervasive (perhaps automated), and difficult to remove vandalism directly
* can target malicious users directly without restricting everyone (<-> page protection)
* historically faster and more reliable, by being a direct part of the core software
* people fed up with bot governance

---

## Q2: How are edit filters governed?

* EN Wikipedia: edit filter managers (and helpers, special permissions); on other language versions: administrators have the additional permissions
* the group is small, difficult to obtain the permission: 154 edit filter managers (and 1181 admins) (at least 232 bot operators and 6130 users who have the rollback permission)
* implement filters suggested by others or conceived based on observations of current entries in the AbuseLog, other input
* best practice guidelines (test, step-wise application of severer actions; no filters for trivial errors or page specific problems)
* severer actions implemented for current trends and disabled as soon as the wave ebbs away

---

## Q3: Which type of tasks do filters take over?

- overall distribution
<img src="images/general-stats-donut.png" class="stretch" height="500" alt="There are 954 edit filters on EN Wikipedia: roughly 21% of them
are active, 16% are disabled, and 63% are deleted">
<small>There are 954 edit filters on EN Wikipedia: roughly 21% of them are active, 16% are disabled, and 63% are deleted</small>
* in total most filters are hidden: so implemented with the purpose of taking care of
  cumbersome vandalism by specific malicious users

- actions distribution
<img src="images/all-active-filters-actions.png" class="stretch" height="500" alt="Filter actions for all enabled filters">
<small>Filter actions for all enabled filters</small>

<img src="images/active-public-actions-big.png" class="stretch" height="500" alt="Filter actions for enabled public filters">
<small>Filter actions for enabled public filters</small>

<img src="images/active-hidden-actions-big.png" class="stretch" height="500" alt="Filter actions for enabled hidden filters">
<small>Filter actions for enabled hidden filters</small>

- manual tags distribution
<img src="images/manual-tags-distribution-all-filters.png" class="stretch" height="500" alt="Distribution of manually assigned labels for all filters">
<img src="images/manual-tags-distribution-enabled-filters.png" class="stretch" height="500" alt="Distribution of manually assigned labels for enabled filters">
* vandalism/good faith/maintenance <- briefly explain coding?

---

## Q4: How has filters' use changed over time?

<img src="images/filter-hits-zoomed.png" class="stretch" height="500" alt="Number of filter hits per month, Mar 2009-Jan 2019">
<img src="images/filter-hits-manual-tags.png" class="stretch" height="500" alt="Number of filter hits per month, according to manuall assigned labels">
<img src="images/filter-hits-editor-actions.png" class="stretch" height="500" alt="Number of filter hits per month, according to causing editor's action">
<img src="images/filter-hits-actions.png" class="stretch" height="500" alt="Number of filter hits per month, according to filter action">
<img src="images/reverts.png" class="stretch" height="500" alt="Number of reverts per month, Jul 2001-Jul 2017">

Notes:
* filter hit numbers are of the same magnitude as reverts (way higher than
  initially expected)
* beginning: more good faith, later more vandalism hits (somewhat unexpected)
* surge in 2016 and a subsequently higher baseline in hit numbers
 (explaination?)
* overall number of active filters stays the same (condition limit)
* most active filters of all times are quite stable through the years

---

## Directions for future studies

* Comparison across different language versions: different types of governance? Different quality control ecosystems?
* Study hidden filters
* Ethnographic analysis: Repercussions on (new) editors ($1/5$ of all active filters discriminate against new users via the \verb|!("confirmed" in user_groups)| pattern.); To implement a bot or to implement a filter?

Notes:
* What are the differences between how filters are governed on EN Wikipedia
  compared to other language versions?: Different Wikipedia language versions each have a local community behind them.
    These communities vary, sometimes significantly, in their modes of organisation and values.
    It would be very insightful to explore disparities between filter governance and the types of filters implemented between different language versions.
* What are the repercussions on affected editors?
  An ethnographic study of the consequences of edit filters for editors whose edits are filtered. Do they experience frustration or alienation? Do they understand what is going on? Or do they experience for example edit filters' warnings as helpful and appreciate the hints they have been given and use them to improve their collaboration?
* To implement a bot or to implement a filter?: An ethnographic inquiry into if an editor is simultaneously an edit filter manager and a bot operator when faced with a new problem, how do they decide which mechanism to employ for the solution?

--- 

## Contributions

* initial systematic investigation of filters and their role among other mechanisms
* codebook for classifying filters according to their task
* openly accessible artefacts: scripts for data analysis, etc.
* questions for future research

---

# Thank you!

These slides are licensed under the [CC BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

![by](images/Cc-by_new_white.png)
![sa](images/Cc-sa_white.png)

---

# Questions? Comments? Thoughts?

