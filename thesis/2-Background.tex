\chapter{Quality-Control Mechanisms on Wikipedia}
\label{chap:background}

The present chapter studies the scientific literature on Wikipedia's quality control mechanisms in order to better understand the role of edit filters in this ecosystem.

Before 2009, academic studies on Wikipedia tended to ignore algorithmic agents altogether.
The number of their contributions to the encyclopedia was found to be low and therefore their impact was considered insignificant \cite{KitChiBrySuhMyt2007}.
This has gradually changed since around 2009 when the first papers specifically dedicated to bots (and later semi-automated tools such as Huggle and Twinkle) were published.
In 2010, Geiger and Ribes insistently highlighted that the scientific community could no longer neglect these mechanisms as unimportant or noise in the data~\cite{GeiRib2010}.

For one, the mechanisms' relative usage has continued to increase since they were first introduced~\cite{Geiger2009}.
What is more, Geiger and Ribes argue, the algorithmic quality control mechanisms change the system not only in a matter of scale (using bots/tools is faster, hence more reverts are possible) but in a matter of substance: the very way everything interacts with each other is transformed~\cite{GeiRib2010}.
On the grounds of quality control specifically, the introduction of algorithmic mechanisms was fairly revolutionary:
They enabled efficient patrolling of articles by users with little to no knowledge about the particular topic.
Thanks to Wikipedia's idiosyncratic software architecture, this is possible even in the most ``manual'' quality control work (i.e. using watchlists to patrol articles):
Representing information changes via diffs allows editors to quickly spot content that deviates from its immediate context~\cite{GeiRib2010}.

Others were worried it was getting increasingly untransparent how the encyclopedia functions and not only ``[k]eeping traces obscure help[ed] the powerful to remain in power''~\cite{ForGei2012},
but entry barriers for new users were gradually set higher~\cite{HalGeiMorRied2013}:
They had to learn to interact with a myriad of technical tools, learn wiki syntax, but also navigate their ground in a complex system with a decentralised socio-technical mode of governance~\cite{Geiger2017}.
Ford and Geiger even cite a case in which an editor was not sure whether a person deleted their articles or a bot~\cite{ForGei2012}.

Quality control mechanisms on Wikipedia can be categorised into following three groups according to their level of automation: fully automated, semi-automated, and manual.
Fully automated tools include bots, the edit filters, which are the focus of the present thesis, and other MediaWiki's features such as the mechanism for page protection~\cite{Mediawiki:PageProtection} (which allows restricting editing of a particular page to certain usergroups),
and title~\cite{Mediawiki:TitleBlacklist} and spam~\cite{Mediawiki:SpamBlacklist} blacklists which operate on regular expression basis to disallow specific titles or publication of some links perceived as spam.
There is also the automatically functioning Wikipedian machine learning framework ORES~\cite{ORES} which computes quality scores per article or revision.
ORES has a somewhat different status compared to the other technologies listed here:
It is a meta tool whose scores can be employed by other mechanisms.
Semi-automated tools still need some sort of a user interaction/confirmation in order to operate.
In this category fall the tools Huggle~\cite{Wikipedia:Huggle}, Twinkle~\cite{Wikipedia:Twinkle} and STiki~\cite{Wikipedia:STiki}.
There are also some semi-automated bots (although most prominent anti-vandalism bots discussed here are fully automated).
Manual quality control work is done by human editors without the help of any particular software program.

The following sections discuss what the scientific community already knows about the different mechanisms in order to be able to situate edit filters in Wikipedia's quality control ecosystem.

\section{Automated}

\subsection{Bots}
\label{section:bots}

According to the literature, bots constitute the first ``line of defence'' against malicious edits~\cite{GeiHal2013}.
They are also undoubtedly the quality control mechanism studied most in-depth by the scientific community.

Geiger and Ribes~\cite{GeiRib2010} define bots as
``fully-automated software
agents that perform algorithmically-defined tasks involved
with editing, maintenance, and administration in Wikipedia''
\footnote{Not all bots are completely automated:
There are batch scripts started manually and there are also bots that still need a final click by a human.
However, the ones the present work focuses on—the rapid response anti-vandalism agents such as ClueBot NG~\cite{Wikipedia:ClueBotNG} and XLinkBot~\cite{Wikipedia:XLinkBot}—work in a fully automated fashion.}.

Different aspects of bots and their involvement in quality control have been investigated:
In the paper referenced above, the researchers employ their method of trace ethnography (more on it in chapter~\ref{chap:methods}) to follow a disrupting editor around Wikipedia and comprehend the measures taken in collaboration by bots (ClueBot~\cite{Wikipedia:ClueBot} and HBC AIV helperbot7~\cite{Wikipedia:HBCAIVHelperbot}) as well as humans using semi-automated tools (Huggle~\cite{Wikipedia:Huggle} and Twinkle~\cite{Wikipedia:Twinkle}) up until they achieved that the malicious editor in question was banned~\cite{GeiRib2010}.
Halfaker and Riedl offer a historical review of bots and semi-automated tools and their involvement in vandal fighting~\cite{HalRied2012}, assembling a comprehensive list of tools and touching on their working principle (rule vs. machine learning based).
They also develop a bot taxonomy classifying bots in one of the following three groups according to their task area: content injection, monitoring or curating; augmenting MediaWiki functionality; and protection from malicious activity.
In~\cite{GeiHal2013}, Geiger and Halfaker conduct an in-depth analysis of ClueBot NG, ClueBot's machine learning based successor, and its place within Wikipedia's vandal fighting infrastructure concluding that quality control on Wikipedia is a robust process and most malicious edits eventually get reverted even with some of the actors (temporaly) inactive, although at a different speed.
They discuss the mean times to revert of different mechanisms, their observations coinciding with figure~\ref{fig:funnel-no-filters},
and also comment on the (un)realiability of external infrastructure bots rely upon (run on private computers, which causes downtimes).

Further bots involved in vandal fighting (besides ClueBot~\cite{GeiRib2010} and ClueBot NG~\cite{GeiHal2013}, \cite{HalRied2012}) discussed by the literature include:
XLinkBot (which reverts edits containing links to domains blacklisted as spam)~\cite{HalRied2012},
HBC AIV Helperbots (responsible for various maintenance tasks which help to keep entries on the Administrator intervention against vandalism (AIV) dashboard up-to-date)~\cite{HalRied2012}, \cite{GeiRib2010},
MartinBot \cite{Wikipedia:MartinBot} and AntiVandalBot \cite{Wikipedia:AntiVandalBot} (one of the first rule-based bots which detected obvious cases of vandalism) \cite{HalRied2012},
DumbBOT \cite{Wikipedia:DumbBOT} and EmausBot \cite{Wikipedia:EmausBot} (which do batch cleanup tasks) \cite{GeiHal2013}.

Very crucial for the current analysis will also be Livingstone's observation in the preamble to his interview with the first large scale bot operator Ram-man that
``[i]n the Wikimedia software, there are tasks that do all sorts of things [...].
If these things are not in the software, an external bot could do them. [...]
The main difference is where it runs and who runs it''~\cite{Livingstone2016}.
This thought is also scrutinised by Geiger~\cite{Geiger2014} who examines in detail what the difference and repercussions are of code that is part of the core software and code that runs alongside it (such as bots) which he calls ``bespoke code''.
Geiger pictures Wikipedia as a big socio-technical assemblage of software pieces and social processes, often completely untransparent for an outside observer who is not able to identify the single components of this system and how they interact with one another to provide the end result to the public.
He underlines that components which are not strictly part of the server-side codebase but run by various volunteers (which is well true for the most parts of Wikipedia, it is a community project) on their private infrastructure constitute the major part of Wikipedia and also that they can experience a downtime at any moment. %TODO this may have been largely true in 2014, but there is a trend towards centralisation (bots run on the toolserver, etc).
The vital tasks they perform, such as vandalism fighting, are often taken for granted, much to their developers' aggravation.

%Concerns
A final aspect in the bot discussion relevant here are the concerns of the community.
People have been long sceptical (and some still are) about the employment of fully automated agents such as bots within Wikipedia (some has called this fear ``botophobia''~\cite{Geiger2011}).
Above all, there is a fear of bots (especially such with admin permissions) running rampant and their operators not reacting fast enough to prevent the damage.
This led to the social understanding that ``bots ought to be better behaved than people''~\cite{Geiger2011} which still plays a crucial role in bot development today.

\subsection{ORES}

ORES~\cite{ORES} is an API based free libre and open source (FLOSS) machine learning service ``designed to improve the way editors maintain the quality of Wikipedia'' \cite{HalTar2015} and increase the transparency of the quality control process.
It uses learning models to predict a quality score for each article and edit based on edit/article quality assessments manually assigned by Wikipedians.
Potentially damaging edits are highlighted, which allows editors who engage in vandal fighting to examine them in greater detail.
The service was officially introduced in November 2015 by Aaron Halfaker\footnote{\url{https://wikimediafoundation.org/role/staff-contractors/}} (principal research scientist at the Wikimedia Foundation
\footnote{The Wikimedia Foundation is a non-profit organisation dedicated to collecting and disseminating free knowledge~\cite{Wikimedia:Mission}.
Beside Wikipedia, it provides and maintains the infrastructure for a family of projects such as Wikimedia Commons (a collection of freely usable media), Wiktionary (a free dictionary), or Wikidata (a free structured knowledge base)~\cite{Wikimedia:Projects}.}
) and Dario Taraborelli\footnote{\url{http://nitens.org/taraborelli/cv}} (Head of Research at Wikimedia Foundation at the time)~\cite{HalTar2015}.
Its development is ongoing, coordinated and advanced by Wikimedia's Scoring Platform team.
Since ORES is API based, in theory a myriad of services can be developed that use the predicted scores, or new models can be trained and made available for everyone to use.
As already mentioned, the tool has a meta status, since it does not fight vandals on its own, but rather it can be employed by other mechanisms for determining the probability that a particular edit is disruptive.
The Scoring Platform team reports that popular quality control tools such as Huggle (see next section) have already adopted ORES scores for the compilation of their queues~\cite{HalTar2015}.
What is unique about ORES is that all the algorithms, models, training data, and code are public, so everyone (with sufficient knowledge of the matter) can scrutinise them and reconstruct what is going on.
Halfaker and Taraborelli express the hope that ORES would help hone quality control mechanisms on Wikipedia, and by decoupling the damage prediction from the actual decision how to deal with an edit make the encyclopedia more welcoming towards newcomers.
This last aim is crucial, since there is a body of research demonstrating how reverts in general~\cite{HalKitRied2011} and reverts by (semi-)automated quality control mechanisms in particular drive new editors away~\cite{HalGeiMorRied2013}.
Present authors also signal that these tools still tend to reject the majority of newcomers' edits as made in bad faith.
The researchers also warn that wording is tremendously important for the perception of edits and people who authored them: labels such as ``good'' or ``bad'' are not helpful.

%TODO Concerns?

\subsection{Page Protection, TitleBlacklist, SpamBlacklist}
\label{sec:page-protection}

Page protection is a MediaWiki template-based functionality which allows administrators to restrict edit access to a particular page temporarily (the most common periods are 7 and 30 days) or permanently~\cite{Wikipedia:PageProtection}, \cite{GeiHal2017}.
The mechanism is suitable for handling a higher number of incidents concerning single pages~\cite{Wikipedia:EditFilter}.
Only one study dedicated specifically to page protection on Wikipedia was found—\cite{HillShaw2015}.
In this paper, Hill and Shaw maintain that the mechanism is highly configurable: available in more than ten varieties including the most popular ``full protection'' (only administrators can edit) and ``semi-protection'' (only registered, autoconfirmed users can edit).
Moreover, it is found that pages are protected for various reasons, e.g. to prevent edit warring or vandalism; to enforce a policy or the law; it is an established process to protect articles on the front page.
The researchers also look into the historical development of protected pages on Wikipedia and discuss the repercussions of the mechanism for affected users~\cite{HillShaw2015}.
If a user doesn't have the permissions needed to edit protected page, the ``edit'' link is simply not displayed at all.

The rule-based MediaWiki extensions TitleBlacklist~\cite{Mediawiki:TitleBlacklist} and SpamBlacklist~\cite{Mediawiki:SpamBlacklist} are employed for disallowing disruptive page titles or link spam.
The only more extensive account found on these mechanisms discusses link spam on Wikipedia and has identified the SpamBlacklist as the first mechanism to get activated in the spam removal pipeline~\cite{WestChaVenSokLee2011}.


\section{Semi-Automated}
\label{section:semi-automated}

Semi-automated quality control tools are similar to bots in the sense that they provide automated detection of potential low-quality edits.
The difference however is that with semi-automated tools humans do the final assessment and decide what happens with the edits in question.

There is a scientific discussion of several tools:
Huggle~\cite{Wikipedia:Huggle}, which is probably the most popular and widely used one, is studied in~\cite{GeiHal2013},~\cite{HalRied2012}, and \cite{GeiRib2010}.
Another very popular tool, Twinkle~\cite{Wikipedia:Twinkle}, is commented on by~\cite{GeiHal2013},~\cite{GeiRib2010}, and~\cite{HalGeiMorRied2013}.
STiki~\cite{Wikipedia:STiki} is presented by its authors in~\cite{WestKanLee2010} and also discussed by~\cite{GeiHal2013}.
Various older (and partially inactive) applications are mentioned by the literature as well:
Geiger and Ribes~\cite{GeiRib2010} touch on Lupin's Anti-vandal tool~\cite{Wikipedia:LupinAntiVandal},
Halfaker and Riedl talk about VandalProof~\cite{HalRied2012}.

Some of these tools are more automated than others: Huggle and STiki for instance are able to revert an edit, issue a warning to the offending editor, and post a report on the AIV dashboard (if the user has already exhausted the warning limit) upon a single click.
The javascript based browser extension Twinkle on the other hand adds contextual links to other parts of Wikipedia which facilitates fulfillment of particular tasks such as rollback multiple edits, report problematic users to AIV, or nominate an article for deletion~\cite{GeiRib2010}.
The main feature of Huggle and STiki is that they both compile a central queue of potentially harmful edits for all their users to check.
The difference between both programs are the heuristics they use for their queues:
By default, Huggle sends edits by users with warnings on their user talk page to the top of the queue, places edits by IP editors higher and ignores edits made by bots and other Huggle users altogether\cite{GeiRib2010}.
In contrast, STiki relies on the ``spatio-temporal properties of revision metadata''~\cite{WestKanLee2010} for deciding the likelihood of an edit to be vandalism.
Huggle's queue can be reconfigured, however, some technical savvy and motivation is needed for this and thus, as~\cite{GeiRib2010} warn, it makes certain paths of action easier to take than others.
Another common trait of both programs is that as a standard, editors need the ``rollback'' permission in order to be able to use them~\cite{HalRied2012}. %TODO another source is STiki's doc

Some critique that has been voiced regarding semi-automated anti-vandalism tools compares these to massively multiplayer online role-playing games (also known as MMORPGs)~\cite{HalRied2012}.
The concern is that some of the users of said tools see themselves as vandal fighters on a mission to slay the greatest number of monsters (vandals) possible and by doing so to excell in the ranks
\footnote{STiki actually has a leader board: \url{https://en.wikipedia.org/w/index.php?title=Wikipedia:STiki/leaderboard&oldid=905145147}}.
This is for one a harmful way to view the project, neglecting the ``assume good faith'' guideline~\cite{Wikipedia:GoodFaith}
and also leads to such users seeking out easy to judge instances from the queues in order to move onto the next entry more swiftly and gather more points
leaving more subtle cases which really require human judgement to others.

Transparency wise, one can criticise that the heuristics they use to compile the queues of potential malicious edits in need of attention are oftentimes obfuscated by the user interface and so the editors using them are not aware why exactly these and not other edits are displayed to them.
The heuristics to use are configurable to an extent, however, one needs to be aware of this option~\cite{GeiRib2010}. %TODO maybe move to pitfalls/concerns discussion


%TODO review this concern as well!
\begin{comment}
\cite{HalGeiTer2014}
"Tools like Huggle raise practical design challenges and eth-
ical issues for HCI researchers. In previous work, we have
critiqued the “professional vision”[17] they enact and the as-
sumptions and values they embody: most tools situate users
as police, not mentors, affording rejection and punishment."
\end{comment}

\begin{comment}
%Huggle
Huggle was initially released in 2008.
In order to use Huggle, editors need the ``rollback'' permission~\cite{HalRied2012}.
Huggle presents a pre-curated queue of edits to the user which can be classified as vandalism by a single mouse click which simultaneously takes action accordingly: the edit is reverted, the offending editor is warned~\cite{HalRied2012}.
Moreover, Huggle is able to parse the talk page of the offending user where warnings are placed in order to issue a next warning of suitable degree and also makes automated reports to AIV (Administrators Intervention Against Vandalism, explain!) if the user has exhausted the warning limit.
The software uses a set of heuristics for compiling the queue with potentially offending edits.
The defaults include placing higher edits containing large removal of content or complete blankings of a page, edits made by anonymous users or users whose edits have been reverted in the past.
Edits by users with warnings on their user talk page are sent to the top of the queue, while edits made by bots and other Huggle users are ignored altogether\cite{GeiRib2010}.
One can reconfigure the queue, however, some technical savvy and motivation is need for this and thus, as~\cite{GeiRib2010} warn, it makes certain paths of action easier to take than others.

%STiki
STiki was introduced by Andrew G. West in June 2010~\footnote{\url{https://en.wikipedia.org/wiki/Wikipedia:STiki}}.
Its defining characteristic is relying on ``spatio-temporal properties of revision metadata''~\cite{WestKanLee2010} for deciding the likelihood of an edit to be vandalism.
According to the authors, this makes the tool's vandalism detection more robust and language independent.
One of the following conditions must be fulfilled for an editor to obtain a permission to use STiki:
(1) they must have the rollback permission, or
(2) they must have made at least 1000 article edits, or
(3) they must have obtained a special permission via their talk page~\footnote{\url{https://en.wikipedia.org/wiki/Wikipedia:STiki}}.

According to~\cite{GeiHal2013} Huggle and STiki complement each other in their tasks, with Huggle users making swifter reverts and STiki users taking care of older edits.

%Twinkle
Twinkle, a javascript based ``user interface extension that runs inside of a standard web browser''~\cite{GeiRib2010} seems to be less automated than the previous tools~\cite{GeiHal2013}.
It adds contextual links to other parts of Wikipedia which facilitates fulfilling particular tasks (rollback multiple edits, report problematic users to AIV, nominate an article for deletion) with a single click~\cite{GeiRib2010}.
A prerequisite for using Twinkle is being an autoconfirmed registered user~\footnote{\url{https://en.wikipedia.org/wiki/Wikipedia:Twinkle}}.


%Lupin's anti-vandal tool
%VandalProof
Older tools which are not much used anymore include Lupin's anti-vandal tool which
``provides a real-time in-browser feed of edits made matching certain algorithms''~\cite{GeiRib2010}
and VandalProof which
``let[s] trusted editors monitor article edits as fast as they happened in Wikipedia and revert unwanted contributions in one click''~\cite{HalRied2012}.
\end{comment}


\section{Manual}

For completion, it should be noted at this point that despite the steady increase of the proportion of fully and semi-automated tools usage for fighting vandalism~\cite{Geiger2009}, some of the quality control work is still done ``manually'' by human editors.
These are, on one hand, editors who use the ``undo'' functionality from within the page's revision history.
On the other hand, there are also editors who engage with the classic encyclopedia editing mechanism (click the ``edit'' button on an article, enter changes in the dialog which opens, write an edit summary for the edit, click ``save'') rather than using further automated tools to aid them.
When Wikipedians use these mechanisms for vandalism fighting, oftentimes they haven't noticed the vandalising edits by chance but rather have been actively watching the pages in question via the so-called watchlists~\cite{AstHal2018}.
This also gives us a hint as to what type of quality control work humans take over: less obvious and less rapid, requiring more complex judgement~\cite{AstHal2018}.
Editors who patrol pages via watchlists often have some relationship to/deeper expertise on the topic.

\section{Conclusion}

For clarity, the various aspects of algorithmic quality control mechanisms learnt by studying related works are summarised in table~\ref{table:mechanisms-comparison-literature}.
Their work can be fittingly illustrated by figure~\ref{fig:funnel-no-filters}, proposed in a similar fashion also by~\cite{AstHal2018}.
What strikes about this diagram is that it foregrounds the temporal dimension of quality control work done on Wikipedia demonstrating that as a general rule bots are the first mechanisms to intercept a potentially harmful edit, less obviously disruptive edits are often caught by semi-automated quality control tools and really subtle cases are uncovered by manually reviewing humans or sometimes not at all.

One thing is certain: So far, on grounds of literature review alone, it remains unclear what the role of edit filters is.
The mechanism is ostentatiously missing from the studied accounts.
In the remainder of the current thesis, I try to remedy this gap in research by exploring following questions:\\
Q1: What is the role of edit filters among existing algorithmic quality-control mechanisms on Wikipedia (bots, semi-automated tools, ORES, humans)?\\
%-- chapter 4 (and 2)
Q2: Edit filters are a classical rule-based system. Why are they still active today when more sophisticated ML approaches exist?\\
%-- chapter 6 (discussion)
Q3: Which type of tasks do filters take over?\\ %-- chapter 5
Q4: How have these tasks evolved over time (are there changes in the type, number, etc.)? %-- chapter 5 (can be significantly expanded)

In order to be able to answer them, various Wikipedia's pages, among other things policies, guidelines, documentation and discussions, are studied in chapter~\ref{chap:filters} and filter data from the English Wikipedia is analysed in chapter~\ref{chap:overview-en-wiki}.
But first, chapter~\ref{chap:methods} introduces the applied methodology.

%TODO is it better to introduce the graphic earlier?
\begin{landscape}
\begin{figure}
\centering
  \includegraphics[width=0.9\columnwidth]{pics/funnel-no-filters-new.png}
  \caption{State of the scientific literature: edit filters are missing from the quality control ecosystem}~\label{fig:funnel-no-filters}
\end{figure}
\end{landscape}

%TODO check which entries actually result from the text!!
% and get rid of the empty page that follows
\begin{landscape}
{\small
    \begin{longtable}{ | p{2cm} | p{3cm} | p{3cm} | p{3cm} | p{3cm} | p{3cm} }
    \hline
               & Page protection & Blacklists & Bots & Semi-Automated tools & ORES \\
    \hline
        \multirow{7}{*}{Properties} & per page & rule-based & rule/ML based & rule/ML based & ML framework \\
        & & & & & \\
                               & part of MediaWiki & part of MediaWiki & run on user's infrastructure ("bespoke code") & extra infrastructure & not used directly, can be incorporated in other tools \\
        & & & & & \\
                               & MediaWiki is open source & & no requirement for code to be public & most popular are open source (but not a hard requirement) & open source \\
        & & & & & \\
                               & & & & heuristics obfuscated by the interface & \\
        & & & & & \\
                               & one cannot edit at all & trigger before an edit is published & latency varies (ClueBot NG needs only a couple of seconds)& generally higher latency than bots & \\
        & & & & & \\
                               & & & mostly single dev/operator (recently: bot frameworks) & few devs & few devs \\
    \hline
        Necessary permissons & admin access & admin access & no special rights needed (except for admin bots) & \emph{rollback} permission needed &  \\
         & & & & & \\
                             & & & bot gets a ``bot flag'' & & \\
    \hline
        People involved & admins & admins (anyone can report problems) &  bot operators  & editors with \emph{rollback} permission & mostly Scoring Platform team \\
    \hline
        Hurdles to participate & & & get approval from the BAG & get a \emph{rollback} permission& \\
        & & & & & \\
        & &  understand regexes & programming knowledge, understand APIs, ... & get familiar with the tool & understand ML \\
    \hline
        \multirow{2}{*}{Concerns} & & & ``botophobia'' & gamification & general ML concerns: hard to understand \\
    \hline
        Areas of application & problems with single page & abusive titles/ link spam & mostly obvious vandalism & less obvious cases that require human judgement & \\
    \hline
    \caption{Wikipedia's algorithmic quality control mechanisms in comparison}~\label{table:mechanisms-comparison-literature}
\end{longtable}
}
\end{landscape}
