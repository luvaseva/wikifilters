% ---------------------------------------------------
% ----- Conclusion of the template
% ----- for Bachelor-, Master thesis and class papers
% ---------------------------------------------------
%  Created by C. Müller-Birn on 2012-08-17, CC-BY-SA 3.0.
%  Freie Universität Berlin, Institute of Computer Science, Human Centered Computing.
%
\chapter{Conclusion}
\label{chap:conclusion}

%Summary
The present thesis conducted an initial inquiry into an important quality control mechanism on Wikipedia previously unexplored by the scientific community—the edit filters.
The role of edit filters in Wikipedia's quality control ecosystem, the tasks the filters take care of, as well as some historical trends in filters' usage were studied.
It was further discussed why such an old-school rule-based technology is still actively used today when more advanced machine learning approaches exist.
Additionally, interesting paths for future research were suggested.

% TODO more detailed overview of results
Summing up the most prominent results, edit filters, together with page protection and title/spam blacklist mechanisms, are the first mechanism verifying incoming contributions.
By acting on unpublished edits they can disallow unconstructive ones directly and thus reduce the workload for other mechanisms.
At the time of their introduction, the need was felt for a mechanism that swiftly prohibited obvious but difficult to remove vandalism, often caused by the same highly motivated malicious users.
Although mass-scale page moves to nonsensical names could be taken care of by admin bots, edit filters were viewed as a neater solution since this way such edits are not published at all.
Also, with some dissatisfaction with bots' development processes (poorly tested and not available source code, low responsiveness of some bot operators), the opportunity for a clean start with a new tool was taken.
Apart from targeting single highly motivated disrupting editors, edit filters take care of ``common newbie mistakes'' such as publishing text not formatted according to wikisyntax or erasing an entire page instead of properly moving it to a different name, or suggesting it to the formal Articles for Deletion process.
By issuing warnings with helpful pointers towards possible alternative actions, edit filters allow a unintentionally disrupting editor to improve their contribution before re-submitting it.
With feedback provided immediately at publication, the revert first-ask questions later approach of other mechanisms (which frustrates and alienates good intentioned newcomers~\cite{HalGeiMorRied2013}) is inverted.
Compared to machine learning techniques, rule-based systems such as the edit filters have the advantage of providing higher amount of control for their operators and being easier to use and understand which also enhances accountability.


% TODO Refer back to title! Who is allowed to publish? Who decides?
Taking a step back,
according to the Wikipedian community, people adding made-up information like references to Brazilian aardvarks or proclaiming themselves mayors of small Chinese towns~\cite{Wikipedia:ChenFang} shall preferably not publish at all.
If this type of disruption is to be handled with edit filters, two approaches seem feasible:
Warn editors adding the information that their contribution does not contain any references, or outright disallow such edits
(which does not solve the problem of freely invented sources),
but that was pretty much it.
Albeit edit filters may not be the ideal mechanism to deal with hoaxes, what they can do effectively is prevent someone from moving hundreds of pages to titles containing ``on wheels''~\cite{Wikipedia:OnWheelsVandalism}, thus sparing vandal fighters the need to track down and undo these changes, allowing them to use their time more productively by for example fact checking unverified claims and hence reducing the number of fake aardvarks and increasing the overall credibility of the project.

%Outlook: centralisation, censorship
It is impressive how in under 20 years ``a bunch of nobodies created the
world's greatest encyclopedia'' to quote new media researcher Andrew Lih~\cite{Lih2009}.
This was possible, among other things, because there was one Wikipedia to which everybody contributed.
As the project and its needs for quality control grew, a lot of processes became more centralised~\cite{HalGeiMorRied2013}.
It is, at the end, easier to maintain power and control in a centralised infrastructure.
However, centralisation facilitates not only the contribution of everyone towards a common goal—creating the world's biggest knowledge database, but also control.
It is not an accident that at the very introduction of the AbuseFilter extension, critical voices expressed the concern that a really powerful secret tool was created to which very few people were to have access and thereby a large-scale censorship infrastructure was being installed~\cite{Wikipedia:EditFilterTalkArchive1}.
If there were multiple comparable projects, all of them had to be censored in order to silence people.
With Wikipedia being the first go-to source of information for a vast quantity of people all over the world today, the debate whose knowledge is included and who decides what knowledge is worth preserving is essential~\cite{Tkacz2014}.
In the present moment, it is more relevant than ever:
In March 2019, the European Parliament basically voted the introduction of upload filters all over the Internet~\cite{EUParliament:Copyright2019}.
In a way, that is exactly what Wikipedia's edit filters are—they are triggered prior to publication and are able to effectively disallow upload of undesired content.

Since Wikipedia is distinctly relevant for the shaping of public opinion, despite its ``neutral point of view'' policy~\cite{Wikipedia:NeutralPointOfView} it is inherently political.
At the beginnings of this research, I heard from a former colleague that there was an edit filter on the German Wikipedia targeting gendering.
``To gender'' is a linguistic praxis whereby words referring to people are explicitly marked to designate more genders than the standardly used generic masculine.
It is a political praxis aiming to uncover under-represented groups and their experiences through the conscious use of language.
Even though no linguistic norm has established gendering to date, conscious decisions for or against the praxis are political, and so are technologies implementing these decisions.
As it turned out, no such filter existed on the German Wikipedia
\footnote{Although, during one of the monthly WomenEdit meetups~\cite{Wikipedia:WomenEdit} hosted at Wikimedia Deutschland office, women active in the German Wikipedia community related that there was a strong general backlash against gendering. The community is also extremely male dominated.}.
This illustrates a point though:
Artefacts do have politics~\cite{Winner1980} and as Lawrence Lessig puts it, it is up to us to decide what values we embed in the systems we create~\cite{Lessig2006}. %TODO Do Artefacts have politics?

%TODO reuse this?
\begin{comment}
``Code 2.0 TO WIKIPEDIA, THE ONE SURPRISE THAT TEACHES MORE THAN EVERYTHING HERE.'' reads one of the inscriptions of Lawrence Lessig's ``Code Version 2.0'' (p.v)~\cite{Lessig2006}.
And although I'm not quite sure what exactly Lessig meant by this regarding the update of his famous book, I readily agree that Wikipedia is important because it teaches us stuff.
Not only in the literal sense, because it is, well, an encyclopedia.
Being an open encyclopedia, which has grown to be one of the biggest open collaborative projects in the world, studying its complex governance, community building and algorithmic systems can teach us a lot about other, less open systems.
\end{comment}

\begin{comment}
\cite{Lessig2006}
"When we know what values we
want to preserve, we need only be creative about how to preserve them." (p.165)

    %TODO put this somewhere, fun fact
This year the filters have a 10 year anniversary^^

% Values, Lessig! --> check copyright blogpost
% think about what values we embed in what systems and how; --> Lessig (and also Do Artifacts Have Politics)

\cite{GeiRib2010}
"We should not fall into the trap of speaking of bots and
assisted editing tools as constraining the moral agency of
editors"

\end{comment}

%************************************************************************

\begin{comment}
\section{The bigger picture: Upload filters}

The planned introduction of upload filters by the EU copyright reform is seen critically by Wikimedia Germany:
\begin{figure}
\centering
  \includegraphics[width=0.9\columnwidth]{pics/Blackout_of_wikipediade_by_Wikimedia_Deutschland_-_March_2019.png}
  \caption{Blackout of wikipedia.de by Wikimedia Deutschland}~\label{fig:blackout-upload-filters}
\end{figure}

via
\url{https://de.wikipedia.org/wiki/Abschaltung_der_deutschsprachigen_Wikipedia_am_21._M%C3%A4rz_2019#/media/File:Blackout_of_wikipedia.de_by_Wikimedia_Deutschland_-_March_2019.png}

see also
\url{https://wikimediafoundation.org/2019/03/20/four-wikipedias-to-black-out-over-eu-copyright-directive/}
"Volunteer editor communities in four language Wikipedias—German, Czech, Danish, and Slovak—have decided to black out the sites on 21 March in opposition to the current version of the proposed EU Copyright Directive.

Those language editions of Wikipedia will redirect all visitors to a banner about the directive, blocking access to content on Wikipedia for 24 hours. "
"These independent language communities decided to black out in the same way most decisions are made on Wikipedia—through discussion and consensus, "

and
\url{https://wikimediafoundation.org/2019/02/28/we-do-not-support-the-eu-copyright-directive-in-its-current-form-heres-why-you-shouldnt-either/}

timeline
\url{https://edri.org/upload-filters-status-of-the-copyright-discussions-and-next-steps/}

\url{https://en.wikipedia.org/wiki/Directive_on_Copyright_in_the_Digital_Single_Market#Positions}
%*******************************+++
Interesting fact: there are edit filters that try to precisely identify the upload of media violating copyrights

From talk archive:
"Automatic censorship won't work on a wiki. " // so, people already perceive this as censorship; user goes on to basically provide all the reasons why upload filters are bad idea (Interlanguage problems, no recognition of irony, impossibility to discuss controversial issues); they also have a problem with being blocked by a technology vs a real person

Freedom of speech concerns
" Do we think that automatons have the judgement to apply prior restraint to speech? Do we think they should be allowed to do so even if they can be imbued with excellent judgement? We don't allow the government to apply prior restrain to speech, why would we build robots to do it? Laziness?

TheNameWithNoMan (talk) 17:39, 9 July 2008 (UTC)"
%*******************************+++

\cite{Gillespie2010}

Upload filters on YouTube
* censure sexual content
* copyright:
"ContentID, which allows copy-
right owners to automatically search for audio or video they believe matches their intel-
lectual property and automatically issue takedown notices to those users"  // how does this work

"The videos targeted were not only copies of WMG-owned
works, but also amateur videos using their music in the background, or musicians paying
tribute to a band by playing live along with the commercial recording as a backing track"
"This kind of content fingerprinting, being both easy
and oblivious to nuance, encourages these kinds of shotgun tactics." //compare blog post on upload filters
\end{comment}

